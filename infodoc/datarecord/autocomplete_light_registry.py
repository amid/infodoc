# -*- coding: utf-8 -*-
import autocomplete_light

from django.db.models import Q
from infodoc.csmuser.models import *
from infodoc.datarecord.models import *

autocomplete_light.register(
    UserProfile, search_fields=('username', 'first_name', 'last_name'),
    
    autocomplete_js_attributes={'placeholder': u' Почніть набирати прізвище'})
    
autocomplete_light.register(
    Department, search_fields=('number_dep', 'name_department',),
    
    autocomplete_js_attributes={'placeholder': u' Почніть набирати номер або назву відділу'})

class AutocompleteSPR(autocomplete_light.AutocompleteModelBase):
    autocomplete_js_attributes={'placeholder': u'Почніть набирати назву підприємства'}

    def choices_for_request(self):
        q = self.request.GET.get('q', '').replace(u'і',u'i').replace(u'І',u'I')
        choices = self.choices.all()
        if q:
            choices = choices.filter(
            Q(iprukr__icontains=q)| Q(ipr__icontains=q)| Q(npr__icontains=q)
            )

        return self.order_choices(choices)[0:self.limit_choices]

autocomplete_light.register(spr_from_metro, AutocompleteSPR, search_fields = ('npr',))
