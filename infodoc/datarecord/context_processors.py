# -*- coding: utf-8 -*-
from models import *
from django.views.generic import ListView
#~ from django.views.generic.dates import MonthArchiveView, YearArchiveView
#~ from django.http import Http404


class month_filter(ListView):
    model = Correspondence
    template_name = 'datarecord/filter_list.html'
    context_object_name = 'filter_list'

    def get_context_data(self, **kwargs):
        context_data = super(month_filter, self).get_context_data(**kwargs)
        month_list = Correspondence.objects.all().dates('datetime_doc', 'month')[::-1]
        context_data['months_choices'] = month_list
        return context_data
