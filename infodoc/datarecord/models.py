# -*- coding: utf-8 -*-
from django.db import models

from infodoc.csmuser.models import *
from django.core.mail import EmailMessage

from django.db.models import signals
from django.core.mail import send_mail

from django.core import urlresolvers

from django.forms import ModelForm
#~ from django.contrib.auth.models import User

cor_types=((1,u'входящая '), (2,u'исходящая'))
cor_identity=((3,u'местная '), (4,u'иногородняя'))
type_control=(('un_control',u'Не на контролі'), ('control',u'На контролі'), ('del_control',u'Знятий з контролю'))
fix_signature=(('unsigned',u'Не отриманий'), ('partially_signed',u'Отриманий не всіма виконавцями'), ('signed',u'Отриманий'))


class spr_from_metro(models.Model):
    """Підприємства из spr_metro"""
    npr = models.CharField(blank=True, null=True, unique=True, max_length=6, verbose_name="Картка підприємства")
    kok = models.CharField(blank=True, null=True, max_length=50, verbose_name="ЄДРПОУ підприємства")
    iprukr = models.CharField(blank=True, null=True, max_length=255, verbose_name="Найменування підприємства")
    ipr = models.CharField(blank=True, null=True, max_length=255, verbose_name="Найменування підприємства русское")
    
    class Meta:
        verbose_name = 'Предприятия из spr_metro'
        verbose_name_plural = 'Предприятия из spr_metro'
        ordering = ["npr"]
        db_table = 'spr_from_metro'
        permissions = (
        ('perms_spr_metro', u'права на spr_metro'),
        )
    def __unicode__(self):
        return '%s' % self.iprukr


class Correspondence(models.Model):
    """Корреспонденция"""
    datetime_doc = models.DateTimeField(blank=True, verbose_name="Дата реєстрації")
    date_control = models.DateField (blank=True, null=True, verbose_name="Дата зняття з контролю")
    number = models.PositiveIntegerField(blank=True, null=True, verbose_name="Номер документу")
    number_hide = models.PositiveIntegerField(blank=True, null=True)
    number_department = models.ForeignKey('csmuser.Department', blank=True, null=True, verbose_name="Номер відділу")
    number_organization = models.CharField(blank=True, null=True, max_length=10, verbose_name="Номер листа підприємства")
    date_organization = models.DateField (blank=True, null=True, verbose_name="Дата листа підприємства")
    cor_type = models.PositiveIntegerField(choices=cor_types, verbose_name="Тип кореспонденції")
    cor_identity = models.PositiveIntegerField(choices=cor_identity, verbose_name="Приналежність кореспонденції")
    name_pidpr = models.ForeignKey('datarecord.spr_from_metro', blank=True, null=True, verbose_name="Найменування організації")
    new_pidpr = models.CharField(blank=True, null=True, max_length=150, verbose_name="Найменування нової організації")
    theme_cor = models.CharField(blank=True, null=True, max_length=50, verbose_name="Стислий зміст")
    registrator = models.ForeignKey('csmuser.UserProfile', related_name = 'registrator', null=True, blank=True)
    performer = models.ManyToManyField('csmuser.UserProfile', blank=True, null=True, verbose_name="Виконавці")
    checked_email = models.BooleanField(verbose_name="Відмітка про отримання по e-mail")
    control_types = models.CharField(choices=type_control, max_length=25, verbose_name="Контроль документа", default='un_control')
    signature = models.CharField(choices=fix_signature, max_length=25, verbose_name="Відомості про отримання документа", default='unsigned')
    #~ control = models.BooleanField(verbose_name="Відмітка про контроль документа")
    #~ unchecked_control = models.BooleanField(verbose_name="Відмітка про зняття з контролю")
    #~ signature = models.BooleanField(verbose_name="Відмітка про отримання документа")
    fio_signature = models.ManyToManyField('csmuser.UserProfile', related_name = 'fio_signature', blank=True, null=True, verbose_name="ПІБ, які отримали документ")
    OBJECT_DELETE = models.BooleanField(verbose_name="Відмітити для видалення", default=False)
    
    class Meta:
        verbose_name = 'Корреспонденция'
        verbose_name_plural = 'Корреспонденция'
        ordering = ["-datetime_doc"]
        permissions = (
        ('perms_cor_all', u'права общие'),
        ('perms_cor_performer', u'права исполнителей'),
        ('perms_cor_kanc', u'права канцелярии'),
        )
    
    def number_full(self):
        if self.number_department and self.checked_email:
            return '%s/%s (email)' % (self.number, self.number_department.number_dep)
        if self.number_department:
            return '%s/%s' % (self.number, self.number_department.number_dep)
        if self.number and self.checked_email:
            return str(self.number) +u' (email)'
        else:
            return self.number
    
    def number_dep(self):
        return self.number_department.number_dep
    
        #~ if self.number_department and self.checked_email:
            #~ return str(self.number) +u'/'+str(self.number_department) +u' (email)'
        #~ if self.number_department:
            #~ return str(self.number) +u'/'+str(self.number_department)
        #~ if self.number and self.checked_email:
            #~ return str(self.number) +u' (email)'
        #~ else:
            #~ return self.number
            
    def __unicode__(self):
        return '%s' % self.theme_cor;
    
    #~ def save(self):
        #~ for item in UserProfile.objects.all():
            #~ to_email = item.email
            #~ subject = 'бла-бла-бла'
            #~ html_content = '<a href="http://127.0.0.1/edit/%s/">%s</a>' % (self.id, self.theme_cor)
            #~ from_email = 'i@developtolive.com'
            #~ msg = EmailMessage(subject, html_content, from_email, [to_email])
            #~ msg.content_subtype = "html"
            #~ msg.send()
        #~ super(Correspondence, self).save()

#~ def notify_admin(sender, instance, created, **kwargs):
    #~ '''о добавлении нового документа'''
    #~ if created:
       #~ subject = 'New user created'
       #~ message = 'User %s was added' % instance.username
       #~ from_addr = 'no-reply@example.com'
       #~ recipient_list = ('krytopes@gmail.com',)
       #~ send_mail(subject, message, from_addr, recipient_list)
       #~ 
#~ signals.post_save.connect(notify_admin, sender=Correspondence)
