# -*- coding: utf-8 -*-
from models import *
#~ from infodoc.csmuser.models import *
from infodoc.datarecord.models import *
from django import forms

import autocomplete_light

import datetime
from datetime import *
from django.forms.widgets import RadioSelect
from django.forms.extras.widgets import SelectDateWidget
from django.db.models import Max


class forms_incoming(ModelForm):
    def __init__(self, *args, **kwargs):
        super(forms_incoming, self).__init__(*args, **kwargs)
        self.fields['number_hide'].widget=forms.HiddenInput()
        self.fields['number'].widget=forms.HiddenInput()
        self.fields['registrator'].widget=forms.HiddenInput()
        self.fields['signature'].widget=forms.HiddenInput()
        self.fields['number_department'].widget=forms.HiddenInput()
        #~ self.fields['number'].widget.attrs['readonly'] = True
        #~ self.fields['date_doc'].widget=forms.TextInput(attrs={'id':'datepicker'})
        self.fields['datetime_doc'].widget=forms.DateTimeInput(format='%d.%m.%Y %H:%M:%S')
        self.fields['datetime_doc'].initial=datetime.now()
        self.fields['number_organization'].initial='б/н'
        self.fields['date_organization'].widget=forms.TextInput(attrs={'id':'datepicker', 'data-date-format':'dd.mm.yyyy'})
        self.fields['cor_type'].initial=1
        self.fields['cor_type'].widget=forms.HiddenInput()
        #~ self.fields['cor_type'].widget=forms.RadioSelect(choices=cor_types)
        #~ self.fields['cor_identity'].initial=3
        self.fields['cor_identity'].widget=forms.RadioSelect(choices=cor_identity)
        self.fields['control_types'].widget=forms.RadioSelect(choices=type_control)
        self.fields['theme_cor'].widget=forms.TextInput(attrs={'size':'40'})
        self.fields['theme_cor'].required = True
        self.fields['name_pidpr'].required = True
        
    class Meta:
        widgets = autocomplete_light.get_widgets_dict(Correspondence)
        model = Correspondence

class forms_outgoing(ModelForm):
    def __init__(self, *args, **kwargs):
        super(forms_outgoing, self).__init__(*args, **kwargs)
        self.fields['registrator'].widget=forms.HiddenInput()
        self.fields['number_organization'].widget=forms.HiddenInput()
        self.fields['checked_email'].widget=forms.HiddenInput()
        #~ self.fields['number'].widget.attrs['readonly'] = True
        #~ self.fields['date_doc'].widget=forms.TextInput(attrs={'id':'datepicker'})
        self.fields['datetime_doc'].widget=forms.DateTimeInput(format='%d.%m.%Y %H:%M:%S')
        self.fields['datetime_doc'].initial=datetime.now()
        self.fields['date_organization'].widget=forms.TextInput(attrs={'id':'datepicker', 'data-date-format':'dd.mm.yyyy'})
        self.fields['cor_type'].initial=2
        self.fields['cor_type'].widget=forms.HiddenInput()
        self.fields['signature'].widget=forms.HiddenInput()
        self.fields['control_types'].widget=forms.HiddenInput()
        self.fields['cor_identity'].widget=forms.RadioSelect(choices=cor_identity)
        self.fields['theme_cor'].widget=forms.TextInput(attrs={'size':'40'})
        self.fields['theme_cor'].required = True
        self.fields['number_department'].required = True
        #~ self.fields['name_pidpr'].required = True

    class Meta:
        widgets = autocomplete_light.get_widgets_dict(Correspondence)
        model = Correspondence
        

class view_doc(ModelForm):
    def __init__(self, *args, **kwargs):
        super(view_doc, self).__init__(*args, **kwargs)
        self.fields['number'].widget.attrs['readonly'] = True
        #~ self.fields['datetime_doc'].widget.attrs['readonly'] = True
        self.fields['datetime_doc'].widget=forms.DateTimeInput(format='%d.%m.%Y %H:%M:%S')
        self.fields['date_organization'].widget=forms.TextInput(attrs={'id':'datepicker', 'data-date-format':'dd.mm.yyyy'})
        #~ self.fields['cor_type'].widget.attrs['readonly'] = True
        self.fields['cor_type'].widget=forms.HiddenInput()
        #~ self.fields['registrator'].widget=forms.HiddenInput()
        #~ self.fields['fio_signature'].widget=forms.HiddenInput()
        self.fields['signature'].widget=forms.HiddenInput()
        self.fields['number_hide'].widget=forms.HiddenInput()
        #~ self.fields['cor_type'].widget=forms.RadioSelect(choices=cor_types)
        self.fields['cor_identity'].widget=forms.RadioSelect(choices=cor_identity)
        self.fields['control_types'].widget=forms.RadioSelect(choices=type_control)
        self.fields['theme_cor'].widget=forms.TextInput(attrs={'size':'40'})
        
    class Meta:
        widgets = autocomplete_light.get_widgets_dict(Correspondence)
        model = Correspondence
        

class signature_preview(ModelForm):
    def __init__(self, *args, **kwargs):
        super(signature_preview, self).__init__(*args, **kwargs)
        #~ self.fields['theme_cor'].widget = forms.ImageField()
    class Meta:
        model = Correspondence


class reports(forms.Form):
    date_first = forms.DateField(label='Выберите дату', widget=forms.TextInput(attrs={'id':'datepicker'}))
    date_out = forms.DateField(label='Выберите дату', widget=forms.TextInput(attrs={'id':'datepicker'}))
    vid = forms.ChoiceField(choices=[('all','Все документы'), ('incoming_all','Входящие'), ('outgoing_all','Исходящие')], label='Вид')
    tip = forms.ChoiceField(choices=[('all_doc','Все'), ('zayavka','Заявки'), ('pismo','Письма предприятий')], label='Тип')
