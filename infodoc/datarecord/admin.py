from django.contrib import admin
from models import *
from infodoc.csmuser.admin import *
from infodoc.csmuser.models import *

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

class spr_from_metroAdmin(admin.ModelAdmin):
    list_display = ('npr', 'ipr', 'iprukr')
    search_fields = ('npr', 'iprukr')
    
    
class Correspondence_Admin(admin.ModelAdmin):
    list_display = ('datetime_doc', 'number', 'name_pidpr', 'theme_cor', 'cor_type', 'cor_identity')
    search_fields = ('number', 'theme_cor')

admin.site.register(spr_from_metro, spr_from_metroAdmin)
admin.site.register(Correspondence, Correspondence_Admin)

#~ admin.site.register(UserProfile, UserAdmin) 
