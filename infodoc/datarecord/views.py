# -*- coding: utf-8 -*-
from types import *
from django.db.models import Q
from django.db.models import Max, Count

import datetime 
#~ from datetime import *

import re

from django.conf import settings
import xmpp, time

from django.http import HttpResponse
from django.core.context_processors import csrf
from django.core import serializers
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.views.generic import CreateView, ListView, UpdateView, DetailView
#~ from django.views.generic.date_based import object_detail
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.http import Http404


from django.template.loader import get_template
from django.template import Context


from django.contrib.auth.models import User, Group

from itertools import chain
#~ import simplejson as json
import re

from models import *
from forms import *

from postman.api import pm_broadcast

def main(request):
    return TemplateResponse(request, 'index.html', {})
    
def no_sign(request):
    return TemplateResponse(request, '404/no_sign_404.html', {})

def sign_true(request):
    return TemplateResponse(request, '404/sign_true.html', {})
    
def no_works(request):
    return TemplateResponse(request, '404/no_works_404.html', {})

def view1(request):
    # do some stuff here
    return HttpResponse("some html here")

def view2(request):
    events = UserProfile.objects.all()
    print events
    #~ p = UserProfile.objects.get(username="okt").get_short_name()
    #~ print p
    for e in events:
        pm_broadcast(
        sender=request.user,
        recipients=map(lambda x: x, UserProfile.objects.all()),
        subject='Бла-бла-бла',
        body='Кааа',
        )
        #~ if settings.JABBER_SEND:
    #~ if settings.JABBER_ERROR_NOTIFICATION:
      #~ jid = xmpp.protocol.JID(settings.JABBER_ID)
      #~ cl = xmpp.Client(jid.getDomain(), debug=[])
      #~ conn = cl.connect()
      #~ if conn:
          #~ auth = cl.auth(jid.getNode(), settings.JABBER_PASSWORD,
                         #~ resource=jid.getResource())
          #~ if auth:
              #~ recipient= settings.JABBER_RECIPIENT
              #~ recipients = recipient + settings.JABBER_SERVER
              #~ id = cl.send(xmpp.protocol.Message(recipients, settings.JABBER_ERROR_TEXT))
              # Некоторые старые сервера не отправляют сообщения,
              # если вы немедленно отсоединяетесь после отправки
              #~ time.sleep(1)
    #~ return server_error(request, template_name)
    
    return TemplateResponse(request, '404/no_works_404.html', {})

#~ def view2(request):
   #~ return Message(subject=subject, body=body, sender=sender, recipient=recipient)


# Предпросмотр
class Preview_doc(DetailView):
    model = Correspondence
    template_name = 'datarecord/preview_doc.html'
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_all'))
    def dispatch(self, *args, **kwargs):
        return super(Preview_doc, self).dispatch(*args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        list_performer = list(obj.performer.all().values_list('id', flat=True))
        list_fio_signature = list(obj.fio_signature.all().values_list('id', flat=True))
        print list_performer
        print len(list_performer)
        sign_list = list(obj.fio_signature.all().values_list('id', flat=True))
        print sign_list
        list_obj = len(list_performer)-len(sign_list)
        print list_obj
        if len(sign_list)==0:
            print 111
        elif list_obj==1:
            print 222
        elif list_performer==sign_list:
            print 333
        if request.user.id in list_performer:
            return super(Preview_doc, self).get(request, *args, **kwargs)
        if request.user.id in list_fio_signature:
            return HttpResponseRedirect('/sign_true/')
        else:
            return HttpResponseRedirect('/no_sign/')
    
class SignatureView(DetailView):
    model = Correspondence
    context_object_name = 'sign_preview'
    template_name = 'datarecord/preview_signature.html'
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_performer'))
    def dispatch(self, *args, **kwargs):
        return super(SignatureView, self).dispatch(*args, **kwargs)
    
    def get_success_url(self):
        obj = self.get_object()
        try:
            if obj.cor_type==1 and obj.cor_identity==3:
                return '/list_incoming/1/3/'
            if obj.cor_type==1 and obj.cor_identity==4:
                return '/list_incoming/1/4/'
            if obj.cor_type==2 and obj.cor_identity==3:
                return '/list_outgoing/2/3/'
            if obj.cor_type==2 and obj.cor_identity==4:
                return '/list_outgoing/2/4/'
        except:
            raise Http404
    
    def get(self, request, *args, **kwargs):
        l = request.user.groups.values_list('name',flat=True)
        print l
        obj = self.get_object()
        #~ print request.user.id
        #~ print obj.registrator.id
        list_performer = list(obj.performer.all().values_list('id', flat=True))
        print list_performer
        print len(list_performer)
        sign_list = list(obj.fio_signature.all().values_list('id', flat=True))
        print sign_list
        list_obj = len(list_performer)-len(sign_list)
        print list_obj
        if len(sign_list)==0:
            print 111
        elif list_obj==1:
            print 222
        elif list_performer==sign_list:
            print 333
            #~ obj.signature = 'control'
        if request.user.id in sign_list:
            return HttpResponseRedirect('/sign_true/')
        if request.user.id in list_performer:
            return super(SignatureView, self).get(request, *args, **kwargs)
        else:
            return HttpResponseRedirect('/no_sign/')
    
    def post(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            obj = self.get_object()
            list_performer = list(obj.performer.all().values_list('id', flat=True))
            print list_performer
            sign_list = list(obj.fio_signature.all().values_list('id', flat=True))
            print sign_list
            list_obj = len(list_performer)-len(sign_list)
            if len(list_performer)==1:
                print 333
                obj.fio_signature.add(self.request.user)
                obj.signature = 'signed'
                obj.save()
                return HttpResponseRedirect(self.get_success_url())
            elif list_obj==1:
                print 222
                obj.fio_signature.add(self.request.user)
                obj.signature = 'signed'
                obj.save()
                return HttpResponseRedirect(self.get_success_url())
            elif len(sign_list)==0 or len(sign_list)<len(list_performer):
                obj.signature = 'partially_signed'
                obj.fio_signature.add(self.request.user)
                obj.save()
                return HttpResponseRedirect(self.get_success_url())
            
            return super(SignatureView, self).get(request, *args, **kwargs)
        else:
            raise Http404
            
class VoidView(DetailView):
    # типа анулированние документа
    model = Correspondence
    template_name = 'datarecord/void.html'
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_kanc'))
    def dispatch(self, *args, **kwargs):
        return super(VoidView, self).dispatch(*args, **kwargs)
    
    def get_success_url(self):
        obj = self.get_object()
        try:
            if obj.cor_type==1 and obj.cor_identity==3:
                return '/list_incoming/1/3/'
            if obj.cor_type==1 and obj.cor_identity==4:
                return '/list_incoming/1/4/'
            if obj.cor_type==2 and obj.cor_identity==3:
                return '/list_outgoing/2/3/'
            if obj.cor_type==2 and obj.cor_identity==4:
                return '/list_outgoing/2/4/'
        except:
            raise Http404
    
    def get(self, request, *args, **kwargs):
        #return user
        user_kanc = UserProfile.objects.filter(groups__name='Kanc').values_list('id', flat=True)
        if request.user.is_superuser or self.request.user.id in user_kanc:
            return super(VoidView, self).get(request, *args, **kwargs)
        else:
            return HttpResponseRedirect('/no_sign/')
    
    def post(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            obj = self.get_object()
            obj.OBJECT_DELETE = True
            obj.save()
            return HttpResponseRedirect(self.get_success_url())
            return super(SignatureView, self).get(request, *args, **kwargs)
        else:
            raise Http404

class EditView(UpdateView):
    model = Correspondence
    form_class = view_doc
    template_name = 'datarecord/edit.html'
    
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_kanc'))
    def dispatch(self, *args, **kwargs):
        return super(EditView, self).dispatch(*args, **kwargs)
    
    def get_success_url(self):
        obj = self.get_object()
        if obj.cor_type==1 and obj.cor_identity==3:
            return '/list_incoming/1/3/'
        if obj.cor_type==1 and obj.cor_identity==4:
            return '/list_incoming/1/4/'
        if obj.cor_type==2 and obj.cor_identity==3:
            return '/list_outgoing/2/3/'
        if obj.cor_type==2 and obj.cor_identity==4:
            return '/list_outgoing/2/4/'
            
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        # Отправка сообщений во все дыры если, документ редактировался и в нем есть исполнители. Да простят меня все кто будет это читать, что я зашил в код текст сообщений. :(
        if self.object.performer != None:
            list_performer = self.object.performer.all()
            for item in list_performer:
                #по e-mail
                to_email = item.email
                subject = 'Повідомлення від системи'
                html_content = ('Відбулись зміни у вхідному листі з темою ').decode('utf-8')
                html_content += '<a href="http://infodoc.app.local/preview_doc/%s/">%s</a> ' % (self.object.id, self.object.theme_cor)
                html_content += ('в якому ви є виконавцем, який надійшов від %s').decode('utf-8') % (self.object.name_pidpr or self.object.new_pidpr)
                from_email = 'mail@lssmcc.lg.ua'
                msg = EmailMessage(subject, html_content, from_email, [to_email])
                msg.content_subtype = "html"
                msg.send()
                #по типа внутренней почте
                pm_broadcast(
                sender=self.request.user,
                recipients = map(lambda x: x, list_performer),
                subject='Повідомлення від системи',
                body=('Відбулись зміни у вхідному листі, в якому ви є виконавцем, №%s від %s з темою %s, який надійшов від %s').decode('utf-8') % (self.object.number, self.object.datetime_doc.strftime('%d.%m.%Y'), self.object.theme_cor, self.object.name_pidpr or self.object.new_pidpr)
                )
                #в джаббер
                jid = xmpp.protocol.JID(settings.JABBER_ID)
                cl = xmpp.Client(jid.getDomain(), debug=[])
                conn = cl.connect()
                if conn:
                    auth = cl.auth(jid.getNode(), settings.JABBER_PASSWORD, resource=jid.getResource())
            
                    if auth:
                        recipient=item.jabber_name
                        recipients = recipient + settings.JABBER_SERVER
                        print recipients
                        id = cl.send(xmpp.protocol.Message(recipients,
                        ('Відбулись зміни у вхідному листі, в якому ви є виконавцем, №%s від %s з темою %s, який надійшов від %s http://infodoc.app.local/preview_doc/%s/"').decode('utf-8') 
                        % (self.object.number, self.object.datetime_doc.strftime('%d.%m.%Y'), self.object.theme_cor, self.object.name_pidpr or self.object.new_pidpr, self.object.id)
                        ))
        return HttpResponseRedirect(self.get_success_url())
        

class list_incoming(ListView):
    model = Correspondence
    template_name = 'datarecord/list_incoming.html'
    context_object_name = 'list_incoming'
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_all'))
    def dispatch(self, *args, **kwargs):
        return super(list_incoming, self).dispatch(*args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        user_kanc = UserProfile.objects.filter(groups__name='Kanc').values_list('id', flat=True)
        list_display_document = list(Correspondence.objects.all().values_list('performer', flat=True))
        works_list = Correspondence.objects.filter(performer__in=[request.user.id]).values_list('id', flat=True)
        
        if request.user.is_superuser or request.user.id in user_kanc:
            qkwargs = {}
            for key in ['cor_type', 'cor_identity']:
                if kwargs.has_key(key):
                    qkwargs[key] = kwargs[key]
            self.queryset = self.model.objects.filter(**qkwargs)
            print kwargs
            if kwargs.has_key('cor_type'):
                if kwargs['cor_type']==u'1':
                    self.type = 'Вхідна'
            if kwargs.has_key('cor_identity'):
                if kwargs['cor_identity']==u'3':
                    self.identity = 'місцева'
                elif kwargs['cor_identity']==u'4':
                    self.identity = 'іногородня'
            try:
                self.title = '%s - %s' % (
                    getattr(self, 'type', ' Взагалі'),
                    getattr(self, 'identity', 'вся кореспонденція')
                )
            except:
                pass
            if self.title == 'вся - вся':
                self.title = 'Вся'
            return super(list_incoming, self).get(request, *args, **kwargs)
            
        if self.request.user.is_authenticated():
            print request.user.id
            list_display_document = list(Correspondence.objects.all().values_list('performer', flat=True))
        #~ list_display_document = map(lambda x: UserProfile.objects.get(pk=x), Correspondence.objects.all().values_list('performer', flat=True))
            print list_display_document
            if request.user.id in list_display_document:
                qkwargs = {}
                for key in ['cor_type', 'cor_identity']:
                    if kwargs.has_key(key):
                        qkwargs[key] = kwargs[key]
                self.queryset = self.model.objects.filter(performer__in=[request.user.id], **qkwargs)
                print kwargs
                if kwargs.has_key('cor_type'):
                    if kwargs['cor_type']==u'1':
                        self.type = 'Вхідна'
                if kwargs.has_key('cor_identity'):
                    if kwargs['cor_identity']==u'3':
                        self.identity = 'місцева'
                    elif kwargs['cor_identity']==u'4':
                        self.identity = 'іногородня'
                try:
                    self.title = '%s - %s' % (
                        getattr(self, 'type', ' Взагалі'),
                        getattr(self, 'identity', 'вся кореспонденція')
                    )
                except:
                    pass
                if self.title == 'вся - вся':
                    self.title = 'Вся'
                return super(list_incoming, self).get(request, *args, **kwargs)
        
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect('/')
           
    def get_context_data(self, **kwargs):
        context_data = super(list_incoming, self).get_context_data(**kwargs)
        context_data['title'] = self.title
        return context_data

        
class list_outgoing(ListView):
    model = Correspondence
    template_name = 'datarecord/list_outgoing.html'
    context_object_name = 'list_outgoing'
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_all'))
    def dispatch(self, *args, **kwargs):
        return super(list_outgoing, self).dispatch(*args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            qkwargs = {}
            for key in ['cor_type', 'cor_identity']:
                if kwargs.has_key(key):
                    qkwargs[key] = kwargs[key]
            self.queryset = self.model.objects.filter(**qkwargs)
            print kwargs
            if kwargs.has_key('cor_type'):
                if kwargs['cor_type']==u'2':
                    self.type = 'Вихідна'
            if kwargs.has_key('cor_identity'):
                if kwargs['cor_identity']==u'3':
                    self.identity = 'місцева'
                elif kwargs['cor_identity']==u'4':
                    self.identity = 'іногородня'
            try:
                self.title = '%s - %s' % (
                    getattr(self, 'type', ' Взагалі'),
                    getattr(self, 'identity', 'вся кореспонденція')
                )
            except:
                pass
            if self.title == 'вся - вся':
                self.title = 'Вся'
            return super(list_outgoing, self).get(request, *args, **kwargs)
        
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect('/')
           
    
    def get_context_data(self, **kwargs):
        context_data = super(list_outgoing, self).get_context_data(**kwargs)
        context_data['title'] = self.title
        return context_data
        

class DocCreate_incoming(CreateView):
    model = Correspondence
    form_class = forms_incoming
    template_name = 'datarecord/forms_incoming.html' 
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_kanc'))
    def dispatch(self, *args, **kwargs):
        return super(DocCreate_incoming, self).dispatch(*args, **kwargs)
    def get_success_url(self):
        try:
            if self.object.cor_type==1 and self.object.cor_identity==3:
                return '/list_incoming/1/3/'
            if self.object.cor_type==1 and self.object.cor_identity==4:
                return '/list_incoming/1/4/'
            if self.object.cor_type==2 and self.object.cor_identity==3:
                return '/list_outgoing/2/3/'
            if self.object.cor_type==2 and self.object.cor_identity==4:
                return '/list_outgoing/2/4/'
        except:
            return '/404/'
    
    def get(self, request, *args, **kwargs):
        #~ l = Correspondence.objects.filter(performer__in=[request.user.id]).values_list('id', flat=True)
        #~ print self.object.theme_cor
        #~ print l
        #~ user_profile = UserProfile.objects.filter(username = request.user)[0]
        user_profile = request.user.get_full_name()
        print 111
        print user_profile
        
        return super(DocCreate_incoming, self).get(request, *args, **kwargs)
    
    def form_valid(self, form):
        #~ user_profile = self.request.user.get_full_name()
        format = "%Y.%m.%d"
        today_year = datetime(2013, 1, 1)
        next_year = datetime(year=today_year.year+1, month=1, day=1)
        enddate = next_year + timedelta(days=-1)
        self.object = form.save(commit=True)
        if self.object.cor_type==1 and self.object.cor_identity==3:
            max_num=Correspondence.objects.filter(cor_type=1, cor_identity=3).aggregate(Max('number_hide'))['number_hide__max']
            print enddate
            now = datetime.now()
            now_str = now.strftime(format)
            next_year_str = next_year.strftime(format)
            if now_str>=next_year_str:
                max_num = Correspondence.objects.filter(datetime_doc__range=[next_year, enddate]).aggregate(Max('number_hide'))['number_hide__max']
                print max_num
                if max_num == None:
                    max_num = 1
                else:
                    max_num = int(max_num)+1
            if now_str!=next_year_str:
                if max_num == None:
                    max_num = 1
                    print max_num
                else:
                    max_num = int(max_num)+1
            self.object.number_hide = max_num
            self.object.number = max_num
        self.object.registrator = self.request.user
        print form._errors
        self.object.save()
        
        print form._errors
        if self.object.cor_type==1 and self.object.cor_identity==4:
            max_num=Correspondence.objects.filter(cor_type=1, cor_identity=4).aggregate(Max('number_hide'))['number_hide__max']
            now = datetime.now()
            now_str = now.strftime(format)
            next_year_str = next_year.strftime(format)
            if now_str>=next_year_str:
                max_num = Correspondence.objects.filter(datetime_doc__range=[next_year, enddate]).aggregate(Max('number_hide'))['number_hide__max']
                print max_num
                if max_num == None:
                    max_num = 1
                else:
                    max_num = int(max_num)+1
            if now_str!=next_year_str:
                if max_num == None:
                    max_num = 1
                    print max_num
                else:
                    max_num = int(max_num)+1
            self.object.number_hide = max_num
            self.object.number = max_num
            self.object.registrator = self.request.user
            self.object.save()
            
        if self.object.performer !=None:
            list_performer = self.object.performer.all()
            events = UserProfile.objects.all()
            for item in list_performer:
                to_email = item.email
                subject = 'Повідомлення від системи'
                html_content = ('Вас призначено виконавцем у вхідному місцевому листі з темою ').decode('utf-8')
                html_content += '<a href="http://infodoc.app.local/preview_doc/%s/">%s</a> ' % (self.object.id, self.object.theme_cor)
                html_content += ('Який надійшов від %s').decode('utf-8') % (self.object.name_pidpr or self.object.new_pidpr)
                from_email = 'mail@lssmcc.lg.ua'
                msg = EmailMessage(subject, html_content, from_email, [to_email])
                msg.content_subtype = "html"
                msg.send()

                pm_broadcast(
                sender = self.request.user,
                #~ recipients = item.user,
                recipients = map(lambda x: x, list_performer),
                subject='Повідомлення від системи',
                body=('Вас призначено виконавцем у вхідному листі №%s від %s з темою %s, який надійшов від %s').decode('utf-8')
                % (self.object.number, self.object.datetime_doc.strftime('%d.%m.%Y'), self.object.theme_cor, self.object.name_pidpr or self.object.new_pidpr)
                )
                
                jid = xmpp.protocol.JID(settings.JABBER_ID)
                cl = xmpp.Client(jid.getDomain(), debug=[])
                conn = cl.connect()
                if conn:
                    auth = cl.auth(jid.getNode(), settings.JABBER_PASSWORD, resource=jid.getResource())
                    
                    if auth:
                        recipient=item.jabber_name
                        recipients = recipient + settings.JABBER_SERVER
                        print recipients
                        id = cl.send(xmpp.protocol.Message(recipients,
                        ('Вас призначено виконавцем у вхідному листі №%s від %s з темою %s, який надійшов від %s http://infodoc.app.local/preview_doc/%s/"').decode('utf-8') 
                        % (self.object.number, self.object.datetime_doc.strftime('%d.%m.%Y'), self.object.theme_cor, self.object.name_pidpr or self.object.new_pidpr, self.object.id)
                        ))
                
        return HttpResponseRedirect(self.get_success_url())
    

class DocCreate_outgoing(CreateView):
    model = Correspondence
    form_class = forms_outgoing
    template_name = 'datarecord/forms_outgoing.html' 
    @method_decorator(login_required)
    @method_decorator(permission_required('datarecord.perms_cor_kanc'))
    def dispatch(self, *args, **kwargs):
        return super(DocCreate_outgoing, self).dispatch(*args, **kwargs)
    def get_success_url(self):
        try:
            if self.object.cor_type==1 and self.object.cor_identity==3:
                return '/list_incoming/1/3/'
            if self.object.cor_type==1 and self.object.cor_identity==4:
                return '/list_incoming/1/4/'
            if self.object.cor_type==2 and self.object.cor_identity==3:
                return '/list_outgoing/2/3/'
            if self.object.cor_type==2 and self.object.cor_identity==4:
                return '/list_outgoing/2/4/'
        except:
            return '/404/'
    def form_valid(self, form):
        format = "%Y.%m.%d"
        today_year = datetime(2013, 1, 1)
        next_year = datetime(year=today_year.year+1, month=1, day=1)
        enddate = next_year + timedelta(days=-1)
        self.object = form.save(commit=True)
        if self.object.cor_type==2 and self.object.cor_identity==3:
            max_num=Correspondence.objects.filter(cor_type=2, cor_identity=3).aggregate(Max('number_hide'))['number_hide__max']
            print enddate
            now = datetime.now()
            now_str = now.strftime(format)
            next_year_str = next_year.strftime(format)
            if now_str>=next_year_str:
                max_num = Correspondence.objects.filter(datetime_doc__range=[next_year, enddate]).aggregate(Max('number_hide'))['number_hide__max']
                print max_num
                if max_num == None:
                    max_num = 1
                else:
                    max_num = int(max_num)+1
            if now_str!=next_year_str:
                if max_num == None:
                    max_num = 1
                    print max_num
                else:
                    max_num = int(max_num)+1
            self.object.number_hide = max_num
            self.object.number = max_num
            self.object.registrator = self.request.user
            self.object.save()
        if self.object.cor_type==2 and self.object.cor_identity==4:
            max_num=Correspondence.objects.filter(cor_type=2, cor_identity=4).aggregate(Max('number_hide'))['number_hide__max']
            now = datetime.now()
            now_str = now.strftime(format)
            next_year_str = next_year.strftime(format)
            if now_str>=next_year_str:
                max_num = Correspondence.objects.filter(datetime_doc__range=[next_year, enddate]).aggregate(Max('number_hide'))['number_hide__max']
                print max_num
                if max_num == None:
                    max_num = 1
                else:
                    max_num = int(max_num)+1
            if now_str!=next_year_str:
                if max_num == None:
                    max_num = 1
                    print max_num
                else:
                    max_num = int(max_num)+1
            self.object.number_hide = max_num
            self.object.number = max_num
            self.object.registrator = self.request.user
            self.object.save()
        return HttpResponseRedirect(self.get_success_url())

#~ def report_forms(request):
    #~ result1 = []
    #~ result2 = []
    #~ if request.method == 'POST':
        #~ form = reports(request.POST)
        #~ if form.is_valid():
            #~ if form.cleaned_data['vid']=='all' and form.cleaned_data['tip']=='all_doc':
                #~ for item in list(chain(
                    #~ Correspondence.objects.filter(Q(date_doc__gte=form.cleaned_data['date_first']) & Q(date_doc__lte=form.cleaned_data['date_out']))
                    #~ )):
                        #~ if item not in result1:
                            #~ result1.append(item)
                #~ for item in list(chain(
                    #~ Local_documents.objects.filter(Q(date_doc__gte=form.cleaned_data['date_first']) & Q(date_doc__lte=form.cleaned_data['date_out']))
                    #~ )):
                        #~ if item not in result2:
                            #~ result2.append(item)
            #~ if form.cleaned_data['vid']=='incoming_all' and form.cleaned_data['tip']=='all_doc':
                #~ result1 = Correspondence.objects.filter(cor_type__in=[1]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='incoming_all' and form.cleaned_data['tip']=='zayavka':
                #~ result1 = Correspondence.objects.filter(cor_type__in=[1], cor_doc_type__in=[7]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='incoming_all' and form.cleaned_data['tip']=='pismo':
                #~ result1 = Correspondence.objects.filter(cor_type__in=[1], cor_doc_type__in=[8]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='outgoing_all' and form.cleaned_data['tip']=='all_doc':
                #~ result1 = Correspondence.objects.filter(cor_type__in=[2]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='outgoing_all' and form.cleaned_data['tip']=='zayavka':
                #~ result1 = Correspondence.objects.filter(cor_type__in=[2], cor_doc_type__in=[7]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='outgoing_all' and form.cleaned_data['tip']=='pismo':
                #~ result1 = Correspondence.objects.filter(cor_type__in=[2], cor_doc_type__in=[8]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='local_docs' and form.cleaned_data['tip']=='all_doc':
                #~ result2 = Local_documents.objects.filter(Q(date_doc__gte=form.cleaned_data['date_first']) & Q(date_doc__lte=form.cleaned_data['date_out']))
                #~ 
            #~ if form.cleaned_data['vid']=='local_docs' and form.cleaned_data['tip']=='prikaz':
                #~ result2 = Local_documents.objects.filter(local_doc_type__in=[5]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='local_docs' and form.cleaned_data['tip']=='rozpor':
                #~ result2 = Local_documents.objects.filter(local_doc_type__in=[6]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='all' and form.cleaned_data['tip']=='zayavka':
                #~ result1 = Correspondence.objects.filter(cor_doc_type__in=[7]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='all' and form.cleaned_data['tip']=='pismo':
                #~ result1 = Correspondence.objects.filter(cor_doc_type__in=[8]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='all' and form.cleaned_data['tip']=='prikaz':
                #~ result2 = Local_documents.objects.filter(local_doc_type__in=[5]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
            #~ if form.cleaned_data['vid']=='all' and form.cleaned_data['tip']=='rozpor':
                #~ result2 = Local_documents.objects.filter(local_doc_type__in=[6]).filter(
                    #~ Q(date_doc__gte=form.cleaned_data['date_first']) & 
                    #~ Q(date_doc__lte=form.cleaned_data['date_out'])
                #~ )
    #~ else:
        #~ form = reports()
    #~ return render_to_response('reports.html', {
    #~ 'form': form,
    #~ 'result1': result1,
    #~ 'result2': result2
    #~ }, context_instance=RequestContext(request))

    
#Update c последующим Insert (Типа ввод по аналогу)
#~ def update_documents(request):
    #~ a = request.session.get('a',  None)
    #~ print a
    #~ if request.method == 'POST':
        #~ form = <имя формы>(request.POST, instance=a)
        #~ 
        #~ print form
        #~ 
        #~ if form.is_valid():
                #~ j = form.save()
                #~ j.save()
    #~ else:
        #~ form = <имя формы>(instance=a)
    #~ print form._errors
    #~ return render_to_response('<имя шаблона>.html', {'form':form}, context_instance=RequestContext(request))

 #~ for item in list_performer:
                #~ to_email = item.email
                #~ subject = 'o'
                #~ template = get_template('email/email.html')
                #~ context = Context({'performer': id})
                #~ content = template.render(context)
                #~ from_email = 'mail@lssmcc.lg.ua'
                #~ msg = EmailMessage(subject, content, from_email, [to_email])
                #~ msg.send()
