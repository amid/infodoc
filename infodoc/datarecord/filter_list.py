# -*- coding: utf-8 -*-

from models import *
from context_processors import *
import datetime, calendar
from django.views.generic import ListView
from django.views.generic.dates import MonthArchiveView, YearArchiveView
from django.http import Http404
#~ from django.contrib.auth.models import User, Group

from datetime import timedelta
from django.utils import timezone


class by_all(month_filter):

    def get(self, request, *args, **kwargs):
        qkwargs = {}
        self.queryset = self.model.objects.filter(**qkwargs)
        self.title = 'Вся кореспонденція'
        return super(by_all, self).get(request, *args, **kwargs)
            
    def get_context_data(self, **kwargs):
        context_data = super(by_all, self).get_context_data(**kwargs)
        context_data['title'] = self.title
        return context_data

class by_void(month_filter):
    
    def get(self, request, *args, **kwargs):
        user_kanc = UserProfile.objects.filter(groups__name='Kanc').values_list('id', flat=True)
        print user_kanc
        #~ format = "%Y-%m-%d"
        #~ now = datetime.now()
        #~ now_str = now.strftime(format)
        #~ delta = timedelta(days=1)
        #~ yesterday = now - delta
        #~ yesterday_str = yesterday.strftime(format)
        
        if request.user.is_superuser or request.user.id in user_kanc:
            qkwargs = {}
            for key in ['cor_type', 'cor_identity']:
                if kwargs.has_key(key):
                    qkwargs[key] = kwargs[key]
            #~ self.queryset = self.model.objects.filter(datetime_doc__startswith = yesterday_str, OBJECT_DELETE=True, **qkwargs)
            self.queryset = self.model.objects.filter(OBJECT_DELETE=True, **qkwargs)
            print kwargs
            if kwargs.has_key('cor_type'):
                if kwargs['cor_type']==u'1':
                    self.type = 'Анульована вхідна'
            if kwargs.has_key('cor_identity'):
                if kwargs['cor_identity']==u'3':
                    self.identity = 'місцева'
                elif kwargs['cor_identity']==u'4':
                    self.identity = 'іногородня'
            if kwargs.has_key('cor_type'):
                if kwargs['cor_type']==u'2':
                    self.type = 'Анульована вихідна'
            if kwargs.has_key('cor_identity'):
                if kwargs['cor_identity']==u'3':
                    self.identity = 'місцева'
                elif kwargs['cor_identity']==u'4':
                    self.identity = 'іногородня'
            try:
                self.title = '%s %s' % (
                    getattr(self, 'type', 'Анульовані взагалі'),
                    getattr(self, 'identity', 'вся кореспонденція')
                )
            except:
                pass
            if self.title == 'вся - вся':
                self.title = 'Вся'
            return super(by_void, self).get(request, *args, **kwargs)
            
        if not self.request.user.is_authenticated():
            #~ raise Http404
            return HttpResponseRedirect('/')
           
    def get_context_data(self, **kwargs):
        context_data = super(by_void, self).get_context_data(**kwargs)
        context_data['title'] = self.title
        #~ month_list = Correspondence.objects.all().dates('datetime_doc', 'month')[::-1]
        #~ context_data['months_choices'] = month_list
        return context_data

class by_control(month_filter):

    def get(self, request, *args, **kwargs):
        #~ kwargs = { 'datetime_doc__isnull': 1 }
        #~ if kwargs[ 'datetime_doc__isnull' ] == 1:
            #~ print 111
        
        if request.user.is_superuser:
            qkwargs = {}
            for key in ['control_types']:
                if kwargs.has_key(key):
                    qkwargs[key] = kwargs[key]
            self.queryset = self.model.objects.filter(**qkwargs)
            print kwargs
            if kwargs.has_key('control_types'):
                if kwargs['control_types']==u'un_control':
                    self.type = 'Не на контролі'
                elif kwargs['control_types']==u'control':
                    self.type = 'На контролі'
                elif kwargs['control_types']==u'del_control':
                    self.type = 'Зняті з контролю'
            try:
                self.title = '%s' % (
                    getattr(self, 'type', 'бла-бла')
                )
            except:
                pass
            if self.title == 'вся - вся':
                self.title = 'Вся'
            return super(by_control, self).get(request, *args, **kwargs)
            
    def get_context_data(self, **kwargs):
        context_data = super(by_control, self).get_context_data(**kwargs)
        context_data['title'] = self.title
        #~ month_list = Correspondence.objects.all().dates('datetime_doc', 'month')[::-1]
        #~ context_data['months_choices'] = month_list
        return context_data
        

class by_date(MonthArchiveView):
    model = Correspondence
    template_name = 'datarecord/filter_list.html'
    date_field = 'datetime_doc'
    month_format = '%m'
    context_object_name = 'filter_list'
    
    def get_queryset(self):
        if self.request.user.is_authenticated():
            query = super(by_date,self).get_queryset().filter(OBJECT_DELETE=False)
        else:
            raise Http404
        return query

    def get_context_data(self, **kwargs):
        today = datetime.date.today().year
        #~ print datetime.date.today().year + 1
        context = super(by_date, self).get_context_data(**kwargs)
        context['caption'] = '%s - %s.%s' % (u' Записи за', self.get_year(), self.get_month())
        month_list = Correspondence.objects.all().dates('datetime_doc', 'month')[::-1]
        context ['months_choices'] = month_list
        context['title'] = 'Всього'
        return context

class today(month_filter):
    
    def get(self, request, *args, **kwargs):
        format = "%Y-%m-%d"
        today_day = datetime.datetime.today()
        today_str = today_day.strftime(format)
        self.queryset = self.model.objects.filter(datetime_doc__icontains=today_str, **kwargs)
        return super(today, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context_data = super(today, self).get_context_data(**kwargs)
        context_data['title'] = 'Всього'
        return context_data
        
class yesterday(month_filter):
   
    def get(self, request, *args, **kwargs):
        format = "%Y-%m-%d"
        yesterday_day = datetime.datetime.today() - datetime.timedelta(1)
        print yesterday_day
        yesterday_str = yesterday_day.strftime(format)
        self.queryset = self.model.objects.filter(datetime_doc__icontains=yesterday_str, **kwargs)
        return super(yesterday, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context_data = super(yesterday, self).get_context_data(**kwargs)
        context_data['title'] = 'Всього'
        return context_data
        
class week(month_filter):
   
    def get(self, request, *args, **kwargs):
        weeks = timezone.now().date() - timedelta(days=7)
        self.queryset = self.model.objects.filter(datetime_doc__gte=weeks, **kwargs)
        return super(week, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context_data = super(week, self).get_context_data(**kwargs)
        context_data['title'] = 'Всього'
        return context_data
