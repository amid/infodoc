from django import template
from django.utils.encoding import force_unicode 

register = template.Library()

@register.filter
def in_group(user, groups):
    group_list = force_unicode(groups).split(',')
    return bool(user.groups.filter(name__in=group_list).values('name'))
