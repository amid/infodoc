from django import template
from infodoc.datarecord.models import *

register = template.Library()

@register.filter
def incom_4_works(user):
    return Correspondence.objects.filter(cor_type=1, cor_identity=4, performer__in=[user.id]).values_list('id', flat=True)
    
@register.filter
def incom_3_works(user):
    return Correspondence.objects.filter(cor_type=1, cor_identity=3, performer__in=[user.id]).values_list('id', flat=True)
