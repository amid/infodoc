# coding: utf8
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _

# Модифицируем поле email.
# _meta это экземпляр django.db.models.options.Options, который хранит данные о модели.
# Это немного хак, но я пока не нашел более простого способа переопределить поле из базовой модели.
AbstractUser._meta.get_field('email')._unique = True
AbstractUser._meta.get_field('email').blank = False


class Department(models.Model):
    """Наименование отделов"""
    number_dep = models.PositiveIntegerField(max_length=4, verbose_name="Номер відділу", default="")
    name_department = models.CharField(blank=True, null=True, max_length=150, verbose_name="Найменування відділу")
    accessory_department = models.CharField(blank=True, null=True, max_length=50, verbose_name="Приналежність відділу до напрямку")

    class Meta:
        verbose_name = 'Наименование отделов'
        verbose_name_plural = 'Наименование отделов'
        ordering = ['number_dep']
        permissions = (('perms_department', u'права на отделы'),)
    
    def __unicode__(self):
        return str(self.number_dep) +u' - '+self.name_department;
        #~ return '%s' % self.name_department

class UserProfile(AbstractUser):
    patronymic = models.CharField(blank=True, max_length=255, verbose_name="По-батькові")
    position = models.CharField(blank=True, max_length=255, verbose_name="Посада")
    department = models.ForeignKey(Department, blank=True, null=True, related_name = 'depart', verbose_name="Найменування відділу")
    jabber_name = models.CharField(max_length=50, verbose_name="Имя в jabber")
    birthday = models.DateField(_(u'birthday'), blank=True, null=True)

    class Meta:
            verbose_name = 'Сотрудники предриятия'
            verbose_name_plural = 'Сотрудники предриятия'
            ordering = ["username"]
            permissions = (('perms_userprofile', u'права на редактирование сотрудников'),)
    
    def __unicode__(self):  
              return '%s %s %s' % (self.first_name, self.patronymic, self.last_name,)
    
    def get_full_name(self):
        return '%s %s %s' % (self.first_name, self.patronymic, self.last_name,)

    def get_short_name(self):
        return self.username
