# -*- coding: utf-8 -*-

from types import *
from models import *
from forms import *

import re

from django.http import HttpResponse
from django.core.context_processors import csrf
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.views.generic import UpdateView
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.http import Http404

class EditProfile(UpdateView):
    model = UserProfile
    form_class = forms_profile
    context_object_name = 'list_profile'
    template_name = 'profile/my_profile.html'
    
    @method_decorator(login_required)
    @method_decorator(permission_required('csmuser.perms_userprofile'))
    def dispatch(self, *args, **kwargs):
        return super(EditProfile, self).dispatch(*args, **kwargs)
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return HttpResponseRedirect('/')
