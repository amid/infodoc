# coding: utf8
from models import *
from .models import UserProfile
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm


# Допиливаем форму добавления пользователя. В Meta.model указываем нашу модель.
class AdminUserAddForm(UserCreationForm):

    class Meta:
        model = UserProfile
        fields = ("username", "email", "password1", "password2")

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            UserProfile._default_manager.get(username=username)
        except UserProfile.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


# Допиливаем форму редактирования пользователя. В Meta.model указываем нашу модель.
class AdminUserChangeForm(UserChangeForm):

    class Meta:
        model = UserProfile

class forms_profile(ModelForm):
    def __init__(self, *args, **kwargs):
        super(forms_profile, self).__init__(*args, **kwargs)
        #~ self.fields['username'].widget.attrs['readonly'] = True
        self.fields['last_login'].widget=forms.HiddenInput()
        self.fields['username'].widget=forms.HiddenInput()
        self.fields['password'].widget=forms.HiddenInput()
        #~ self.fields['groups'].widget=forms.HiddenInput()
        #~ self.fields['user_permissions'].widget=forms.HiddenInput()
        self.fields['is_superuser'].widget=forms.HiddenInput()
        self.fields['is_staff'].widget=forms.HiddenInput()
        self.fields['is_active'].widget=forms.HiddenInput()
        self.fields['date_joined'].widget=forms.HiddenInput()
    class Meta:
        model = UserProfile
