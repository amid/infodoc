# coding: utf8
from .forms import AdminUserChangeForm, AdminUserAddForm
from .models import *
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _


# Указываем наши формы для создания и редактирования пользователя.
# Добавляем новые поля в fieldsets, и поле email в add_fieldsets.
class UserAdmin(BaseUserAdmin):
    form = AdminUserChangeForm
    add_form = AdminUserAddForm
    list_display = ('username', 'email', 'last_name', 'first_name', 'patronymic', 'position', 'jabber_name', 'is_active', 'is_staff', 'is_superuser',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name',
            'last_name',
            'email',
            'patronymic',
            'position',
            'department',
            'jabber_name',
            'birthday'
        )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')}
        ),
    )

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('number_dep', 'name_department', 'accessory_department')

admin.site.register(UserProfile, UserAdmin)
admin.site.register(Department, DepartmentAdmin)
