import autocomplete_light
autocomplete_light.autodiscover()

from django.conf.urls import patterns, include, url
from django.views import generic
from django.conf.urls.defaults import *
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import RedirectView

from datarecord.views import *
from csmuser.views import *
from datarecord.filter_list import *
from datarecord.context_processors import *
from postman.urls import *
from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import CreateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
     (r'^$', main),
     #~ (r'^forms_incoming/$', forms_correspondence_incoming),
     #~ (r'^forms_outgoing/$', forms_correspondence_outgoing),
     #~ (r'^preview_signature/(?P<name>\d{1,9})/$', preview_signature),
     #~ (r'^list_correspondense/(?P<name>\d{1,9})/$', preview_signature),
     #~ (r'^signature_save/$', signature_save),
     (r'^no_sign/$', no_sign),
     (r'^no_works/$', no_works),
     (r'^sign_true/$', sign_true),
     (r'^view1/$', view1),
     (r'^view2/$', view2),
     (r'^forms_incoming/$', DocCreate_incoming.as_view()),
     (r'^forms_outgoing/$', DocCreate_outgoing.as_view()),
     (r'^create_incoming/$', DocCreate_incoming.as_view()),
     (r'^create_outgoing/$', DocCreate_outgoing.as_view()),
     (r'^edit/(?P<pk>\d{1,9})/$', EditView.as_view()),
     (r'^update/(?P<pk>\d{1,9})/$', EditView.as_view()),
     (r'^edit_profile/(?P<pk>\d{1,9})/$', EditProfile.as_view()),
     (r'^update_profile/(?P<pk>\d{1,9})/$', EditProfile.as_view()),
     (r'^preview_signature/(?P<pk>\d{1,9})/$', SignatureView.as_view()),
     (r'^preview_doc/(?P<pk>\d{1,9})/$', Preview_doc.as_view()),
     (r'^signature_save/(?P<pk>\d{1,9})/$', SignatureView.as_view()),
     (r'^void/(?P<pk>\d{1,9})/$', VoidView.as_view()),
     (r'^void_save/(?P<pk>\d{1,9})/$', VoidView.as_view()),
     (r'^by_void/(?P<cor_type>\d{1})/(?P<cor_identity>\d{1})/$', by_void.as_view()),
     (r'^by_void/(?P<pk>\d{1,9})/$', by_void.as_view()),
     #~ (r'^void_date/(?P<control_types>\w+)/$', FilterVoid_by_date.as_view()),
     (r'^by_control/(?P<control_types>\w+)/$', by_control.as_view()),
     #~ (r'^by_date1/(?P<datetime_doc>\d{1,9})/$', by_date.as_view()),
     url(r'^by_date/(?P<year>\w+)/(?P<month>\w+)/$', by_date.as_view(), name='by_date'),
     (r'^by_all/$', by_all.as_view()),
     (r'^today/$', today.as_view()),
     (r'^yesterday/$', yesterday.as_view()),
     (r'^week/$', week.as_view()),
     (r'^list_incoming/(?P<cor_type>\d{1})/$', list_incoming.as_view()),
     (r'^list_incoming/(?P<cor_type>\d{1})/(?P<cor_identity>\d{1})/$', list_incoming.as_view()),
     (r'^list_outgoing/(?P<cor_type>\d{1})/(?P<cor_identity>\d{1})/$', list_outgoing.as_view()),
     (r'^list_outgoing/(?P<cor_type>\d{1})/$', list_outgoing.as_view()),
     url(r'^accounts/', include('django.contrib.auth.urls')),
     #~ url(r'^accounts/profile/$', RedirectView.as_view(url='/')), 
     url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
     url(r'autocomplete/', include('autocomplete_light.urls')),
     (r'^messages/', include('postman.urls')),
     #~ url(r'^view/(?P<message_id>[\d]+)/$', 'view', {'form_class': MyCustomQuickReplyForm}, name='postman_view'),
     url(r'^admin/', include(admin.site.urls)),
     #~ url(r'widget/add/$', generic.CreateView.as_view(model=Widget, form_class=WidgetForm)),
     #~ url(r'widget/(?P<pk>\d+)/update/$', generic.UpdateView.as_view(model=Widget, form_class=WidgetForm), name='widget_update'),
     #~ url(r'widget/add/$', generic.CreateView.as_view(model=Correspondence, form_class=forms_cor)),
     #~ url(r'widget/(?P<pk>\d+)/update/$', generic.UpdateView.as_view(model=Correspondence, form_class=forms_cor), name='widget_update'),
)


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns('django.contrib.auth.views',
  url(r'^user/(?P<user_id>\d+)/user_edit/password/$', 'password_change')
)
