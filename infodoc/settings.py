# Django settings for infodoc project.
# -*- coding: utf-8 -*-
import os
#~ from django.contrib import admin
SITE_ROOT_DIR= os.path.dirname(os.path.realpath(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)


MANAGERS = ADMINS

TIME_ZONE = 'Europe/Kiev'

LANGUAGE_CODE = 'ru'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(SITE_ROOT_DIR, 'files', 'media')

MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(SITE_ROOT_DIR, 'files', 'static')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(SITE_ROOT_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SECRET_KEY = 'phs8uk(qqdjq%mr)+yd#n9hop(xu8og^vs@6%a-%w+1xoa)fmu'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'infodoc.urls'

try:
    from settings_local import *
except ImportError:
    pass

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'infodoc.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(SITE_ROOT_DIR, 'templates')
)


INSTALLED_APPS = (
    'infodoc.datarecord',
    'autocomplete_light',
    'postman',
    'south',
    'infodoc.csmuser',
    'endless_pagination',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
     'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    
)

AUTH_USER_MODEL = 'csmuser.UserProfile'
AUTH_PROFILE_MODULE='csmuser.UserProfile'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'lssmcc.lugansk@gmail.com'
EMAIL_HOST_PASSWORD = 'comp!347'
EMAIL_PORT = 587

JABBER_ERROR_NOTIFICATION = True
JABBER_ID = 'kanc@10.42.2.4'
JABBER_PASSWORD = '123321'
JABBER_SERVER = '@10.42.2.4'
JABBER_ERROR_TEXT = 'Вас призначено відповідальним для більш детальної інформації увійдіть у систему'

#~ POSTMAN_AUTOCOMPLETER_APP = {'name': '', 'field': '', 'arg_name': '', 'arg_default': 'postman_friends', default is {}  }

#~ AJAX_LOOKUP_CHANNELS = {
    #~ 'postman_users': dict(model='auth.user', search_field='username'),
#~ }
#~ POSTMAN_AUTOCOMPLETER_APP = {
    #~ 'arg_default': 'postman_users',
#~ }

#~ AJAX_LOOKUP_CHANNELS = {
    #~ 'user'  : {'model': 'auth.user', 'search_field': 'username'},
#~ }

#~ AUTHENTICATION_BACKENDS = [
    #~ 'csmuser.UserProfileBackend',
#~ ]

AJAX_LOOKUP_CHANNELS = {
    'user'  : {'model': 'csmuser.UserProfile', 'search_field': 'user'},
}

POSTMAN_AUTOCOMPLETER_APP = {
        'name': 'ajax_select', 
        'field': 'AutoCompleteField',
        'arg_name': 'channel', 
        'arg_default': 'user', # no default, mandatory to enable the feature
    }

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

