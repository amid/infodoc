#-*- encoding:utf-8 -*-
import psycopg2, subprocess
BAD_CODES = ['ў', 'ї', '∙', 'Ў', 'Ї']
GOOD_CODES = ['і', 'є', 'ї', 'І', 'Є']

def convert_dbf(dbf_path, db_host, db_name, table_name_new, new_struct, old_enc, id_year=None, memofile=None, rel_tables=None):

    def dbf_val(row, f_name):
        try:
            if f_name in ['','\N']:
                return f_name
            return row[fields_old.index(f_name)]
        except:
            print 'EXCEPTION', row
            
    conn = psycopg2.connect("host=localhost user=postgres_user password=123 dbname=%s" % (db_name,))
    cur = conn.cursor()
    
    if rel_tables:
        for rel_table in rel_tables:
            if id_year:
                cur.execute('delete from %s where id_year=%s' % (rel_table, id_year,))
            else:
                cur.execute('delete from %s' % (rel_table,))

    if id_year:
        cur.execute('delete from %s where id_year=%s' % (table_name_new, id_year,))
    else:
        cur.execute('delete from %s' % (table_name_new,))

    cur.execute('select max(id) from %s' % table_name_new)
    try:
        id = cur.fetchone()[0]+1
    except:
        id = 1


    sql_dbf_file = open('conv_dbf.sql', 'w')
    bad_enc = False
    
    if old_enc=='bad_ibm866':
        old_enc = 'ibm866'
        bad_enc = True
    if memofile:
        p1 = subprocess.Popen(['pgdbf', dbf_path, '-TD', '-m', memofile], stdout=subprocess.PIPE)
    else:
        p1 = subprocess.Popen(['pgdbf', dbf_path, '-TD'], stdout=subprocess.PIPE)
    p2 = subprocess.call(['iconv', '-c', '-f', old_enc, '-t', 'UTF-8'], stdin=p1.stdout, stdout=sql_dbf_file)

    sql_dbf_file.close()
    sql_dbf = open('conv_dbf.sql', 'r')
    
    if bad_enc:
        data = sql_dbf.read()
        for bad_sym, good_sym in zip(BAD_CODES, GOOD_CODES):
            data = data.replace(bad_sym, good_sym)
            
        sql_dbf.close()
        sql_dbf = open('conv_dbf.sql', 'w')
        sql_dbf.write(data)
        sql_dbf.close()
        sql_dbf = open('conv_dbf.sql', 'r')

    pg_data = open('pg_data.sql','w')

    line = sql_dbf.readline().split('\n')[0]

    while True:
        if line.find("CREATE TABLE")==0:
            fields_old = []
            fields_dbf = line[line.find("(")+1:line.rfind(")")].split(", ")
            
            for field in fields_dbf:
                fields_old.append(field.split(" ")[0])
        else:
            break
            
        line = sql_dbf.readline().split('\n')[0]
    line = sql_dbf.readline().split('\n')[0]
    
    while True:
        row = line.split('\t')
        new_row = []

        if table_name_new not in ['app_sert','app_svid'] or (dbf_val(row,'nsnp')!='' and int(''.join(i for i in dbf_val(row,'nsnp') if i.isdigit()))>0):
            for f_item in new_struct:

                if f_item in fields_old:
                    new_row.append(dbf_val(row,f_item))
                    
                elif f_item=='id':
                    new_row.append(str(id))
                    
                elif f_item=='id_year':
                    new_row.append(id_year)
                    
                elif f_item.find('|')>=0:
                    val1,val2 = f_item.split('|')
                    
                    if dbf_val(row,val1):
                        new_row.append(dbf_val(row,val1))
                    else:
                        new_row.append(dbf_val(row,val2))

                else:
                    f_template = f_item

                    for f in fields_old:

                        f_str = '_%s_' % f
                        
                        if f_template.find(f_str)>=0:
                            
                            f_template = f_template.replace(f_str,dbf_val(row,f))

                    new_row.append(f_template)
                            
                        
            pg_data.write("\t".join(new_row)+'\n')

        #while True:
            #line = sql_dbf.readline().split('\n')[0]
            #if line!='\n':
                #break
        line = sql_dbf.readline().split('\n')[0]
        id+=1
        if line.find('\\.')==0:
            break
        #if line==''
            #line = sql_dbf.readline().split('\n')[0]




    pg_data.close()
    sql_dbf.close()
    pg_data = open('pg_data.sql', 'r')

        
    pg_data = open('pg_data.sql', 'r')
    cur.copy_from(pg_data, table_name_new)
    conn.commit()
    conn.close()
    pg_data.close()
